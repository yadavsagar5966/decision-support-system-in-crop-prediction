# Decision Support System

This reposistory Contains the resource Files of the Android application and Web Application of Decision Support System in Crop Prediction.

**All Files are Available in the Master Branch.**

**Project Members:**
Sagar Yadav,
Chaitanya Tupsamudre,
Mayur Warpe,
Varad Lad,
Omkar Patil.

**Problem Definition:**

“Krishi App for farmers for agricultural decisions using Machine Learning” will be an application that will suggest suitable crops to be cultivated based on various factors such as rainfall, temperature, soil type, soil pH etc.

**Scope:**
To recommend optimum crops to be cultivated by farmers based on various parameters and help them make an decision before cultivation
The application would entail the following modules with the following functionalities:

1.) Web interface: The Web Interface will interact with the farmers to collect information and statistics.

2.) Android app: This android application will interact with farmmers to collect information and predict crops, soil analysis, weather data(using open weather map API), etc

3.) Machine Learning Agent: Prediction based on the dataset.

4.) Chatbot: To perform soil health analysis without soil testing kit.

**Steps to run Android code :**

1.) Install/Open Android Studio

2.) Set up the environment

3.) Clone this repository and download it to your local computer

4.) Navigate to where the folder is downloaded

5.) Go to the folder Decision-Support-System-master

6.) Run the app

7.) Input the values

8.) Check the results obtained


**Steps to run Web application:**

1.) Install Flask,render template,flask_mysqldb,PIL,numpy

2.) Set up the environment and turn on sql server

3.) Clone this repository and download it to your local computer

4.) Navigate to where the folder is downloaded and then import the database named as 'flask_db' in your system for login credentials.

5.) Run the app.py file using the command python app.py in your terminal

6.) select the soil image from your device 

7.) Check the results obtained

8.) Go through Homemade Methods and chatbot present in web application





**Conclusion:** 

We have successfully built a web application and android application which helps to suggest farmers the crops they should intent to grow by taking in various parameters.

