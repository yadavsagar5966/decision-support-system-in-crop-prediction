#app.py
from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL,MySQLdb
import bcrypt 

from PIL import Image, ImageOps
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from werkzeug.utils import secure_filename
import os, sys, glob, re


app = Flask(__name__)
np.set_printoptions(suppress=True) 
app.secret_key = "varadlad_7517"
 
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'flaskdb'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)
 
@app.route('/')
def home():
    return render_template('login.php')
 
@app.route('/register', methods=["GET", "POST"]) 
def register():
    if request.method == 'GET':
        return render_template("register.php")
    else:
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        # hash_password = bcrypt.hashpw(password, bcrypt.gensalt())
 
        curl = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        curl.execute("SELECT * FROM users WHERE email=%s",(email,))
        data = curl.fetchone()
        curl.close()
        if len(data) > 0:
            return render_template("alreadyregistered.php")
            # return "You Have Already registered Please Login" + render_template("login.php")
        else:    
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO users (name, email, password) VALUES (%s,%s,%s)",(name,email,password,))
            mysql.connection.commit()
            session['name'] = request.form['name']
            session['email'] = request.form['email']
            return render_template("registeredsuccessfully.php")
            # return "You Have registered Successfully, Please Login" + render_template("login.php")


@app.route('/login', methods=["GET","POST"])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        
        curl = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        curl.execute("SELECT * FROM users WHERE email=%s",(email,))
        user = curl.fetchone()
        curl.close()
        
        if user is not None:   
            if password == user["password"]:
                session['name'] = user['name']
                session['email'] = user['email']
                return render_template("index.php")
            else:
                return render_template("wrongpassword.php")
                # return "You Entered a Wrong Password" + render_template("login.php") 
        else:
            return render_template("notregistered.php")
            # return "You Have Not registered, Please Register Here" + render_template("register.php")                
    else:
        return render_template("login.php")
 
@app.route('/logout')
def logout():
    session.clear()
    return render_template("login.php")

@app.route('/chatbot')
def chatbot():
    return render_template("index2.html")    
@app.route('/index')
def index():
    return render_template("index.php")

def model_predict(image_path):

    # Load the model
    # model = tensorflow.keras.models.load_model('keras_model.h5')
    model_path = "keras_model.h5"
    mymodel = load_model(model_path)
    # Create the array of the right shape to feed into the keras model
    # The 'length' or number of images you can put into the array is
    # determined by the first position in the shape tuple, in this case 1.
    data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)

    # Replace this with the path to your image
    image = Image.open(request.files['image'])  

    #resize the image to a 224x224 with the same strategy as in TM2:
    #resizing the image to be at least 224x224 and then cropping from the center
    size = (224, 224)
    image = ImageOps.fit(image, size, Image.ANTIALIAS)

    #turn the image into a numpy array
    image_array = np.asarray(image)

    # display the resized image
    # image.show()

    # Normalize the image
    normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1

    # Load the image into the array
    data[0] = normalized_image_array

    # run the inference
    prediction = mymodel.predict(data)
    print(prediction)
    print('\n')
    
    newarr = np.array_split(prediction[0],8)
    print(newarr[0])
    print(newarr[1])
    print(newarr[2])
    print(newarr[3])
    print(newarr[4])
    print(newarr[5])
    print(newarr[6])
    print(newarr[7])     
    #Initialize max with first element of array.    
    max = newarr[0];    
         
    #Loop through the array    
    for i in range(0, len(newarr)):    
        #Compare elements of array with max    
       if(newarr[i] > max):    
           max = newarr[i];    
               
    for i in range(0, len(newarr)):  
        if(max == newarr[i]):
            v=i
    print (v)
      
    if v == 0: 
        return "Alluvial","Alluvial.php"
    elif v == 1:
        return "Black", "Black.php"
    elif v == 2:
        return "Clay" , "Clay.php"
    elif v == 3:
        return "Red" , "Red.php"
    elif v == 4:
        return "Laterite" , "Laterite.php"
    elif v == 5:
        return "Desert" , "Desert.php"     
    elif v == 6:
        return "Mountain" , "Mountain.php"
    elif v == 7:
        return "Saline" , "Saline.php"         

@app.route('/predict',methods=['GET','POST'])
def predict():
    print("Entered")
    if request.method == 'POST':
        print("Entered here")
        file = request.files['image'] # fet input
        filename = file.filename        
        print("@@ Input posted = ", filename)
        
        file_path = os.path.join('static/user uploaded', filename)
        file.save(file_path)

        print("@@ Predicting class......")
        pred, output_page = model_predict(file_path)
              
        return render_template(output_page, pred_output = pred, user_image = file_path)

if __name__ == '__main__':
    app.run(debug=True,threaded=False)   
