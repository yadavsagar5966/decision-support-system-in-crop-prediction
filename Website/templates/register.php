{% block content %}
{% include "navbar.php" %}
<div class="container">
	<div class="row" style="margin-top: 40px;">
		<div class="col-md-4">
			<br>
			<br>
			<h2>Register</h2>
			<hr>
			<form action="/register" method="POST">
				<div class="form-group">
					<label><b>Full Name</b></label>
					<input type="text" class="form-control" name="name" required="" placeholder="Enter Name">
				</div>
				<div class="form-group">
					<label><b>Username</b></label>
					<input type="text" class="form-control" name="email" required="" placeholder="Enter Username">
				</div>
				<div class="form-group">
					<label><b>Password</b></label>
					<input type="password" class="form-control" name="password" required="" placeholder="Enter Password">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" name="register" value="Register">
					&nbsp;&nbsp;&nbsp;
					<a href="/" class="btn">Login Here</a>
				</div>
			</form>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<center>
				<img src="/static/css/images/websiteimages/logo/login2new.png" height="250px" width="350px" style="border-radius: 20px; margin-top: 60px;">
			</center>
		</div>
	</div>
</div>
{% endblock %}