package com.example.decisionsupportsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CompactionActivity extends AppCompatActivity {
    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compaction);
        myDialog = new Dialog(this);
    }
    public void ShowPopup_compaction(View v){
        TextView textclose4;
        myDialog.setContentView(R.layout.compaction_popup);
        textclose4 = (TextView) myDialog.findViewById(R.id.txtclose4);
        textclose4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        myDialog.show();
    }
}