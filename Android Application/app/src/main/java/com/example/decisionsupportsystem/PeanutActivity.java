package com.example.decisionsupportsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PeanutActivity extends AppCompatActivity {

    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peanut);
        myDialog = new Dialog(this);
    }
    public void ShowPopup_peanut(View v){
        TextView textclose1;
        myDialog.setContentView(R.layout.peanut_popup);
        textclose1 = (TextView) myDialog.findViewById(R.id.txtclose1);
        textclose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        myDialog.show();
    }

}