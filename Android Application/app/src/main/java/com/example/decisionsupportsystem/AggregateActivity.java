package com.example.decisionsupportsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AggregateActivity extends AppCompatActivity {

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggregate);
        myDialog = new Dialog(this);
    }
    public void ShowPopup_aggregate(View v){
        TextView textclose3;
        myDialog.setContentView(R.layout.aggregate_popup);
        textclose3 = (TextView) myDialog.findViewById(R.id.txtclose3);
        textclose3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        myDialog.show();
    }
}