package com.example.decisionsupportsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EarthwormActivity extends AppCompatActivity {

    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthworm);
        myDialog = new Dialog(this);
    }
    public void ShowPopup_earthworm(View v){
        TextView textclose2;
        myDialog.setContentView(R.layout.earthworm_popup);
        textclose2 = (TextView) myDialog.findViewById(R.id.txtclose2);
        textclose2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        myDialog.show();
    }
}