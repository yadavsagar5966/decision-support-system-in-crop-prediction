package com.example.decisionsupportsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AcidityActivity extends AppCompatActivity {
    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acidity);
        myDialog = new Dialog(this);
    }
    public void ShowPopup(View v){
        TextView textclose;
        Button btnFollow;
        myDialog.setContentView(R.layout.custompopup);
        textclose = (TextView) myDialog.findViewById(R.id.txtclose);

        textclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        myDialog.show();
    }
}