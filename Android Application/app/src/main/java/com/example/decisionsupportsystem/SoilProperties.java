package com.example.decisionsupportsystem;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class SoilProperties extends AppCompatActivity {

    RelativeLayout peanut,ph,earthworm,stability,compaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soil_properties);
        peanut = (RelativeLayout)findViewById(R.id.peanut_btn);
        ph = (RelativeLayout)findViewById(R.id.ph_btn);
        earthworm = (RelativeLayout)findViewById(R.id.earthworm_btn);
        stability = (RelativeLayout)findViewById(R.id.stability_btn);
        compaction = (RelativeLayout)findViewById(R.id.compaction_btn);
        peanut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoilProperties.this, PeanutActivity.class);
                startActivity(intent);
            }
        });
        ph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoilProperties.this, AcidityActivity.class);
                startActivity(intent);
            }
        });
        earthworm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoilProperties.this, EarthwormActivity.class);
                startActivity(intent);
            }
        });
        stability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoilProperties.this, AggregateActivity.class);
                startActivity(intent);
            }
        });
        compaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoilProperties.this, CompactionActivity.class);
                startActivity(intent);
            }
        });
    }
}