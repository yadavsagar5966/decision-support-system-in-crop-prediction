package com.example.decisionsupportsystem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.decisionsupportsystem.ml.ModelUnquant;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class cropPrediction extends AppCompatActivity {

    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int GALLERY_REQUEST_CODE = 105;
    private Bitmap img;
    ImageView selectedImage;
    RelativeLayout cameraBtn,galleryBtn,button_predict;
    String currentPhotoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_prediction);
        selectedImage = (ImageView) findViewById(R.id.img_soil);
        cameraBtn = (RelativeLayout) findViewById(R.id.camera_btn);
        galleryBtn = (RelativeLayout) findViewById(R.id.gallery_btn);
        button_predict = (RelativeLayout) findViewById(R.id.predict_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Camera button is clicked", Toast.LENGTH_SHORT).show();
                askCameraPermission();
            }
        });
        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Gallery button is clicked", Toast.LENGTH_SHORT).show();
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, GALLERY_REQUEST_CODE);
            }
        });
        button_predict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img = Bitmap.createScaledBitmap(img,224,224,true);
                try {
                    ModelUnquant model = ModelUnquant.newInstance(getApplicationContext());

                    // Creates inputs for reference.
                    TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 224, 224, 3}, DataType.FLOAT32);
                    TensorImage tensorImage = new TensorImage(DataType.FLOAT32);
                    tensorImage.load(img);
                    ByteBuffer byteBuffer = tensorImage.getBuffer();
                    inputFeature0.loadBuffer(byteBuffer);

                    // Runs model inference and gets result.
                    ModelUnquant.Outputs outputs = model.process(inputFeature0);
                    TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

                    // Releases model resources if no longer used.
                    model.close();
                    int ind=0;
                    float max_val = 0.0f;
                    for(int i=0;i<8;i++)
                    {
                        if(outputFeature0.getFloatArray()[i] > max_val)
                        {
                            ind = i;
                            max_val = outputFeature0.getFloatArray()[i];
                        }
                    }

                    if(ind == 0){
                        Toast.makeText(cropPrediction.this, "ALLUVIAL SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 1){
                        Toast.makeText(cropPrediction.this, "BLACK SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 2){
                        Toast.makeText(cropPrediction.this, "CLAY SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 3){
                        Toast.makeText(cropPrediction.this, "RED SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 4){
                        Toast.makeText(cropPrediction.this, "Laterite SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 5){
                        Toast.makeText(cropPrediction.this, "Desert SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else if(ind  == 6){
                        Toast.makeText(cropPrediction.this, "Mountain SOIL", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(cropPrediction.this, "Saline SOIL", Toast.LENGTH_SHORT).show();
                    }
//                    tv.setText("ALLUVIAL SOIL "+outputFeature0.getFloatArray()[0] + "\n BLACK SOIL " + outputFeature0.getFloatArray()[1] + "\n CLAY SOIL " + outputFeature0.getFloatArray()[2]
//                            + "\n RED SOIL " + outputFeature0.getFloatArray()[3]);
                } catch (IOException e) {
                    // TODO Handle the exception
                }
            }
        });
    }
    private void askCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CAMERA}, CAMERA_PERM_CODE);

        } else {
            //openCamera();
            dispatchTakePictureIntent();
        }
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                Intent intent = takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE){
//            Bitmap image = (Bitmap) data.getExtras().get("data");
//            selectedImage.setImageBitmap(image);
            if (resultCode == Activity.RESULT_OK){
                File f = new File(currentPhotoPath);
                selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "Absolute Url of image is"+Uri.fromFile(f));
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                //selectedImage.setImageURI(contentUri);
                try {
                    img = MediaStore.Images.Media.getBitmap(this.getContentResolver(),contentUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        if(requestCode == GALLERY_REQUEST_CODE){
//            Bitmap image = (Bitmap) data.getExtras().get("data");
//            selectedImage.setImageBitmap(image);
            if (resultCode == Activity.RESULT_OK){
                Uri contentUri = data.getData();
                String timeStamp = new  SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
                Log.d("tag","onActivityResult: Gallery Image Uri: "+imageFileName);
                selectedImage.setImageURI(contentUri);

                try {
                    img = MediaStore.Images.Media.getBitmap(this.getContentResolver(),contentUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    private String getFileExt(Uri contentUri) {
        ContentResolver c = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //openCamera();
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required", Toast.LENGTH_SHORT).show();
            }
        }
    }
}