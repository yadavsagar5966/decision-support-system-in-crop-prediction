package com.example.decisionsupportsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {

    private EditText registeremail, registerpassword, registerpassword1, registername;
    private CheckBox chk1;
    private ProgressBar pr1;
    private TextView login_txt;
    private RelativeLayout register_btn;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        registeremail = (EditText) findViewById(R.id.register_email);
        registername = (EditText) findViewById(R.id.register_name);
        registerpassword = (EditText) findViewById(R.id.register_pass1);
        registerpassword1 = (EditText) findViewById(R.id.register_pass2);
        register_btn = (RelativeLayout) findViewById(R.id.register_btn);
        chk1 = (CheckBox) findViewById(R.id.register_checkbox);
        pr1 = (ProgressBar) findViewById(R.id.register_progressbar);
        login_txt = (TextView) findViewById(R.id.login_btn);

        chk1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    registerpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    registerpassword1.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    registerpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    registerpassword1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });


        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = registername.getText().toString();
                String email = registeremail.getText().toString();
                String pass = registerpassword.getText().toString();
                String pass1 = registerpassword1.getText().toString();
                if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(pass) || !TextUtils.isEmpty(pass1) || !TextUtils.isEmpty(name)){
                    if (pass.equals(pass1)) {
                        pr1.setVisibility(View.VISIBLE);
                        mAuth.createUserWithEmailAndPassword(email,pass1).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()){
                                    sendToMain();
                                }else{
                                    String error = task.getException().getMessage();
                                    Toast.makeText(Register.this, "Error: "+error, Toast.LENGTH_SHORT).show();
                                }
                                pr1.setVisibility(View.INVISIBLE);
                            }
                        });
                    }else{
                        Toast.makeText(Register.this, "Confirm password and password field doesn't match", Toast.LENGTH_SHORT).show();
                        pr1.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        login_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this, login.class);
                startActivity(intent);

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            sendToMain();
        }
    }

    private void sendToMain(){
        Intent intent = new Intent(Register.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}